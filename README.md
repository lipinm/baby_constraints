# README #

HW 5.1 pre-proposal:
https://docs.google.com/document/d/1ZvrQ9am781_8fZgbWaBJAxieDWcHQWW4m0F-UxQk7Ao/edit?usp=sharing

HW 5.2 proposal:
https://docs.google.com/document/d/1emZMZ58sL2EP5HpnJJXby07FopzQNw4WR1p483BOL5Q/edit?usp=sharing

HW 5.3 design:
https://docs.google.com/document/d/1enjujn-eHRpLo-mry54mUeowAdfSRVms5K9OMQhxCNc/edit?usp=sharing

HW 5.4 final-design:
https://docs.google.com/document/d/1FlWQjZm7cr5R-TMN2h6a_EdeQqFNm53M1HYR6eBOMQY/edit?usp=sharing

HW 5.5 demo-slides:
https://docs.google.com/presentation/d/1mW28jWmIDiO4oeI9Hj5LSra_8D1mMOFk4HVizYVk_8g/edit?usp=sharing

ScreenCast:
Inside repo. Called Demo.


# Overview #

This project integrates a constraint solver into JavaScript. You can create symbolic ints and produce rules regarding them.
Then retrieve a model that satisfies your rules. Our project can be used to create things like a Sudoku solver.

This project runs on nodev4.6. We have a package.json set up so that when you run npm install from the base folder, node_modules
will fill up accordingly. Inside the examples folder you will find sample code that will look like what you can write. 
Inside src make sure there is a z3.exe, constraints.js, operator.js.

Now how to use your language!
Be sure to do an npm install to get necessary files.
At the start of your javascript, write:
var <baby_constraints_name> = require(<path_to_constraints.js>);
var <solver_name> = <baby_constraints_name>.default();
This unpacks the node packages.

To create new symbolic int call var x = <solver_name>.Int();
Now you have to create rules about your symbolic variables.
you can set equality for your variables with ==
  ex: x == 5
you can also use following other comparisons: <, >, <=, >=
you can add them with +
  ex: x + 6 == 4
you can subtract them with -
  ex: x - y == 5
you can multiply them with *
  ex: x * x == 9
you can divide them with /
  ex: x / 4 == 1

To construct more complicated rules you can also use | and &
they represent or and and(not the bit version)
  ex: x == 4 | x == 3
Include these into your set of rules with <solver_name>.assert(<rule>);

You can also partially fill in some of the variables with <sym_variable>.setVal(<value>).
Don't call this after calling solve.

When ready, call <solver_name>.solve(<callback_func>) at the end.
When the model is ready, the will be called with true if the model was found and false otherwise.
The values of symbolic variable can be accessed with x.getValue() at this point.

The sample tutorials can be run with npm run-script sudoku
(or npm run-script partial_sudoku, npm run-script custom_sudoku, npm run-script algebra depending
on with tutorial)
Look inside ./examples/algebra.js for a commented tutorial.

To run you own code go to this projects root file and call:
node node_modules\\jalangi2\\src\\js\\commands\\jalangi.js --analysis src\\operators.js <path_to_your_file>

After running code. You will see jalangi files in the folder with your code. You will also see a
constraints.smt2 file inside of src. This file is what the z3 input looks like. Try running it on cmd
to get more accurate error catching. It can help you find errors in your rules.