/*
 * Copyright 2014 Samsung Information Systems America, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Author: Koushik Sen

// do not remove the following comment
// JALANGI DO NOT INSTRUMENT

/**
 * @file A template for writing a Jalangi 2 analysis
 * @author  Koushik Sen
 *
 */

(function (sandbox) {
    var code = require("./constraints");
    var lit = code.lit;
    /**
     * <p>
     *     This file is a template for writing a custom Jalangi 2 analysis.  Simply copy this file and rewrite the
     *     callbacks that you need to implement in your analysis.  Other callbacks should be removed from the file.
     *</p>
     *
     * <p>
     *     In the following methods (also called as callbacks) one can choose to not return anything.
     *     If all of the callbacks return nothing, we get a passive analysis where the
     *     concrete execution happens unmodified and callbacks can be used to observe the execution.
     *     One can choose to return suitable objects with specified properties in some callbacks
     *     to modify the behavior of the concrete execution.  For example, one could set the skip
     *     property of the object returned from {@link MyAnalysis#putFieldPre} to true to skip the actual putField operation.
     *     Similarly, one could set the result field of the object returned from a {@link MyAnalysis#write} callback
     *     to modify the value that is actually written to a variable. The result field of the object
     *     returned from a {@link MyAnalysis#conditional} callback can be suitably set to change the control-flow of the
     *     program execution.  In {@link MyAnalysis#functionExit} and {@link MyAnalysis#scriptExit},
     *     one can set the <tt>isBacktrack</tt> property of the returned object to true to reexecute the body of
     *     the function from the beginning.  This in conjunction with the ability to change the
     *     control-flow of a program enables us to explore the different paths of a function in
     *     symbolic execution.
     * </p>
     *
     * <p>
     *     Note that if <tt>process.exit()</tt> is called, then an execution terminates abnormally and a callback to
     *     {@link MyAnalysis#endExecution} will be skipped.
     * </p>
     *
     * <p>
     *     An analysis can access the source map, which maps instruction identifiers to source locations,
     *     using the global object stored in <code>J$.smap</code>.  Jalangi 2
     *     assigns a unique id, called <code>sid</code>, to each JavaScript
     *     script loaded at runtime.  <code>J$.smap</code> maps each <code>sid</code> to an object, say
     *     <code>iids</code>, containing source map information for the script whose id is <code>sid</code>.
     *     <code>iids</code> has the following properties: <code>"originalCodeFileName"</code> (stores the path of the original
     *     script file), <code>"instrumentedCodeFileName"</code> (stores the path of the instrumented script file),
     *     <code>"url"</code> (is optional and stores the URL of the script if it is set during instrumentation
     *     using the --url option),
     *     <code>"evalSid"</code> (stores the sid of the script in which the eval is called in case the current script comes from
     *     an <code>eval</code> function call),
     *     <code>"evalIid"</code> (iid of the <code>eval</code> function call in case the current script comes from an
     *     <code>eval</code> function call), <code>"nBranches"</code> (the number of conditional statements
     *     in the script),
     *     and <code>"code"</code> (a string denoting the original script code if the code is instrumented with the
     *     --inlineSource option).
     *     <code>iids</code> also maps each <code>iid</code> (which stands for instruction id, an unique id assigned
     *     to each callback function inserted by Jalangi2) to an array containing
     *     <code>[beginLineNumber, beginColumnNumber, endLineNumber, endColumnNumber]</code>.  The mapping from iids
     *     to arrays is only available if the code is instrumented with
     *     the --inlineIID option.
     * </p>
     * <p>
     *     In each callback described below, <code>iid</code> denotes the unique static instruction id of the callback in the script.
     *     Two callback functions inserted in two different scripts may have the same iid.  In a callback function, one can access
     *     the current script id using <code>J$.sid</code>.  One can call <code>J$.getGlobalIID(iid)</code> to get a string, called
     *     <code>giid</code>, that statically identifies the
     *     callback throughout the program.  <code>J$.getGlobalIID(iid)</code> returns the string <code>J$.sid+":"+iid</code>.
     *     <code>J$.iidToLocation(giid)</code> returns a string
     *     containing the original script file path, begin and end line numbers and column numbers of the code snippet
     *     for which the callback with <code>giid</code> was inserted.
     *
     * </p>
     * <p>
     *     A number of sample analyses can be found at {@link ../src/js/sample_analyses/}.  Refer to {@link ../README.md} for instructions
     *     on running an analysis.
     * </p>
     *
     *
     *
     * @global
     * @class
     */
    function MyAnalysis() {

        /**
         * This callback is called after a binary operation. Binary operations include  +, -, *, /, %, &, |, ^,
         * <<, >>, >>>, <, >, <=, >=, ==, !=, ===, !==, instanceof, delete, in.
         *
         * @param {number} iid - Static unique instruction identifier of this callback
         * @param {string} op - Operation to be performed
         * @param {*} left - Left operand
         * @param {*} right - Right operand
         * @param {*} result - The result of the binary operation
         * @param {boolean} isOpAssign - True if the binary operation is part of an expression of the form
         * <code>x op= e</code>
         * @param {boolean} isSwitchCaseComparison - True if the binary operation is part of comparing the discriminant
         * with a consequent in a switch statement.
         * @param {boolean} isComputed - True if the operation is of the form <code>delete x[p]</code>, and false
         * otherwise (even if the operation if of the form <code>delete x.p</code>)
         * @returns {{result: *}|undefined} - If an object is returned, the result of the binary operation is
         * replaced with the value stored in the <tt>result</tt> property of the object.
         */
        this.binary = function (iid, op, left, right, result, isOpAssign, isSwitchCaseComparison, isComputed) {
            if ((left != null && left.nodeType) || (right != null && right.nodeType)) {
                if (!(left != null && left.nodeType)) {
                    left = lit(left);
                }
                if (!(right != null && right.nodeType)) {
                    right = lit(right);
                }
                switch(op) {
                    case "==":
                        result = left.equal(right);
                        break;
                    case "+":
                        result = left.plus(right);
                        break;
                    case "-":
                        result = left.minus(right);
                        break;
                    case "*":
                        result = left.times(right);
                        break;
                    case ">":
                        result = left.greater(right);
                        break;
                    case ">=":
                        result = left.greaterEqual(right);
                        break;
                    case "<":
                        result = left.less(right);
                        break;
                    case "<=":
                        result = left.lessEqual(right);
                        break;
                    case "/":
                        result = left.divide(right);
                        break;
                    case "|":
                        result = left.or(right);
                        break;
                    case "&":
                        result = left.and(right);
                        break;
                }
            }
            return {result: result};
        };

        /**
         * This callback is called before a unary operation. Unary operations include  +, -, ~, !, typeof, void.
         *
         * @param {number} iid - Static unique instruction identifier of this callback
         * @param {string} op - Operation to be performed
         * @param {*} left - Left operand
         * @returns {{op: *, left: *, skip: boolean} | undefined} If an object is returned and the
         * <tt>skip</tt> property is true, then the unary operation is skipped.  Original <tt>op</tt> and <tt>left</tt>
         * are replaced with that from the returned object if an object is returned.
         */
        this.unaryPre = function (iid, op, left) {
            return {op: op, left: left, skip: false};
        };

        /**
         * This callback is called after a unary operation. Unary operations include  +, -, ~, !, typeof, void.
         *
         * @param {number} iid - Static unique instruction identifier of this callback
         * @param {string} op - Operation to be performed
         * @param {*} left - Left operand
         * @param {*} result - The result of the unary operation
         * @returns {{result: *}|undefined} - If an object is returned, the result of the unary operation is
         * replaced with the value stored in the <tt>result</tt> property of the object.
         *
         */
        this.unary = function (iid, op, left, result) {
            return {result: result};
        };
    }

    sandbox.analysis = new MyAnalysis();
})(J$);



