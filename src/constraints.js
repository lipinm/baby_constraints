var fs = require("fs");
var cmd = require("node-cmd");

var Constraints = (function () {
	function Solver() {
	  this.intsAllocated = 0;
	  this.symbols = {};
	  this.rules = [];
	}

	Solver.prototype.Int = function() {
	  label = "I" + this.intsAllocated;
	  var Int = new Symbol("Int", label);
	  this.symbols[label] = Int;
	  this.intsAllocated++;
	  return new SymbolNode(Int);
	}

	Solver.prototype.assert = function(rule) {
	  this.rules.push(rule);
	}

	Solver.prototype.solve = function(callback) {
    var symbols = this.symbols;
    var valid = false;
	  var rule = "";
	  for (var i in symbols) {
	    rule += "(declare-const " + i + " " + symbols[i].type + ")\n";
	    if (symbols[i].set) {
	    	rule += "(assert (= " + i + " " + symbols[i].value + "))\n";
	    	symbols[i].clear();
	    }
	  }
	  for (var i = 0; i < this.rules.length; i++) {
	    rule += "(assert " + this.rules[i].print() + ")\n";
	  }
    rule += "(check-sat)\n";
    rule += "(get-model)\n";
    rule += "(exit)\n";
    fs.writeFile("src/constraints.smt2", rule, function(err) {
      if(err) {
        throw err;
      }
    });
    cmd.get("src\\z3.exe src\\constraints.smt2", function(x) {
    	// console.log(x);
      var satline = x.indexOf('\n');
      var isSat = x.substring(0, satline);
      if (isSat.startsWith('sat')) {
        valid = true;
        var match;
        var regex = /\(define-fun (\D\d*).*\s*((?:\(-.)?\d*)/g;
        while (match = regex.exec(x)) {
          var field = match[1];
          if (match[2][0] == '(') {
          	match[2] = '-' + match[2].substring(3);
          }
          var value = parseInt(match[2]);
          symbols[field].value = value;
        }
      }
      callback(valid);
    });
	}

	var lit = function(x) {
	  return new LiteralNode(x);
	}

	function ASTNode(type) {
	  this.nodeType = type;
	}

	ASTNode.prototype = {};

	ASTNode.prototype.print = function() {
	  throw new Error("Unimplemented print for " + this.type + " node");
	}

	function LessEqualNode(left, right) {
		ASTNode.call(this, "LessEqual");
		this.left = left;
		this.right = right;
	}

	LessEqualNode.prototype = Object.create(ASTNode.prototype);

	LessEqualNode.prototype.print = function() {
		return "(<= " + this.left.print() + " " + this.left.print() + ")";
	}

	function GreaterEqualNode(left, right) {
		ASTNode.call(this, "GreaterEqual");
		this.left = left;
		this.right = right;
	}

	GreaterEqualNode.prototype = Object.create(ASTNode.prototype);

	GreaterEqualNode.prototype.print = function() {
		return "(>= " + this.left.print() + " " + this.left.print() + ")";
	}

	function LessThanNode(left, right) {
		ASTNode.call(this, "LessThan");
		this.left = left;
		this.right = right;
	}

	LessThanNode.prototype = Object.create(ASTNode.prototype);

	LessThanNode.prototype.print = function() {
		return "(< " + this.left.print() + " " + this.right.print() + ")";
	}

	function GreaterThanNode(left, right) {
		ASTNode.call(this, "GreaterThan");
		this.left = left;
		this.right = right;
	}

	GreaterThanNode.prototype = Object.create(ASTNode.prototype);

	GreaterThanNode.prototype.print = function() {
		return "(> " + this.left.print() + " " + this.right.print() + ")";
	}

	function OrNode(left, right) {
		ASTNode.call(this, "Or");
		this.left = left;
		this.right = right;
	}

	OrNode.prototype = Object.create(ASTNode.prototype);

	OrNode.prototype.print = function() {
		return "(or " + this.left.print() + " " + this.right.print() + ")";
	}

	function AndNode(left, right) {
		ASTNode.call(this, "And");
		this.left = left;
		this.right = right;
	}

	AndNode.prototype = Object.create(ASTNode.prototype);

	AndNode.prototype.print = function() {
		return "(and " + this.left.print() + " " + this.right.print() + ")";
	}

	function LiteralNode(val) {
	  ASTNode.call(this, "Literal");
	  this.value = val;
	}

	LiteralNode.prototype = Object.create(ASTNode.prototype);

	LiteralNode.prototype.print = function() {
	  return "" + this.value;
	}

	function SymbolNode(symbol) {
	  ASTNode.call(this, "Symbol");
	  this.symbol = symbol;
	}

	SymbolNode.prototype = Object.create(ASTNode.prototype);

	SymbolNode.prototype.setVal = function(val) {
		this.symbol.setVal(val);
	}

	SymbolNode.prototype.print = function() {
	  return this.symbol.label;
	}

	SymbolNode.prototype.getValue = function() {
	  return this.symbol.value;
	}

	function EqualNode(left, right) {
	  ASTNode.call(this, "Equal");
	  this.left = left;
	  this.right = right;
	}

	EqualNode.prototype = Object.create(ASTNode.prototype);

	EqualNode.prototype.print = function() {
	  return "(= " + this.left.print() + " " + this.right.print() + ")";
	}

	function PlusNode(left, right) {
	  ASTNode.call(this, "Plus");
	  this.left = left;
	  this.right = right;
	}

	PlusNode.prototype = Object.create(ASTNode.prototype);

	PlusNode.prototype.print = function() {
	  return "(+ " + this.left.print() + " " + this.right.print() + ")";
	}

	function MinusNode(left, right) {
	  ASTNode.call(this, "Minus");
	  this.left = left;
	  this.right = right;
	}

	MinusNode.prototype = Object.create(ASTNode.prototype);

	MinusNode.prototype.print = function() {
	  return "(- " + this.left.print() + " " + this.right.print() + ")";
	}

	function TimesNode(left, right) {
	  ASTNode.call(this, "Times");
	  this.left = left;
	  this.right = right;
	}

	TimesNode.prototype = Object.create(ASTNode.prototype);

	TimesNode.prototype.print = function() {
	  return "(* " + this.left.print() + " " + this.right.print() + ")";
	}

	function DivideNode(left, right) {
	  ASTNode.call(this, "Divide");
	  this.left = left;
	  this.right = right;
	}

	DivideNode.prototype = Object.create(ASTNode.prototype);

	DivideNode.prototype.print = function() {
	  return "(/ " + this.left.print() + " " + this.right.print() + ")";
	}

	function Symbol(type, label) {
		this.set = false;
	  this.type = type;
	  this.label = label;
	  this.value = null;
	}

	Symbol.prototype.setVal = function(val) {
		this.set = true;
		this.value = val;
	}

	Symbol.prototype.clear = function() {
		this.set = false;
		this.value = null;
	}

	ASTNode.prototype.equal = function(node) {
	  return new EqualNode(this, node);
	}

	ASTNode.prototype.plus = function(node) {
	  return new PlusNode(this, node);
	}

	ASTNode.prototype.minus = function(node) {
	  return new MinusNode(this, node);
	}

	ASTNode.prototype.times = function(node) {
	  return new TimesNode(this, node);
	}

	ASTNode.prototype.divide = function(node) {
	  return new DivideNode(this, node);
	}

	ASTNode.prototype.or = function(node) {
		return new OrNode(this, node);
	}

	ASTNode.prototype.and = function(node) {
		return new AndNode(this, node);
	}

	ASTNode.prototype.greater = function(node) {
		return new GreaterThanNode(this, node);
	}

	ASTNode.prototype.less = function(node) {
		return new LessThanNode(this, node);
	}

	ASTNode.prototype.greaterEqual = function(node) {
		return new GreaterEqualNode(this, node);
	}

	ASTNode.prototype.lessEqual = function(node) {
		return new LessEqualNode(this, node);
	}
	
	return {Solver: function() { return new Solver(); }, lit: lit};
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Constraints.Solver;
exports.lit = Constraints.lit;
