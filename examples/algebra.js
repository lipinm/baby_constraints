// importing and initializing our solver
var code = require("../src/constraints");
var solver = code.default();

// initializing symbolic ints
var x = solver.Int();
var y = solver.Int();
var z = solver.Int();

solver.assert(x == 3 * y + 2 * z);  // making rule: x = 3y + 2z
solver.assert(y < x / 2 & y > z);   // making rule: z < y < x / 2
solver.assert(z == x - 20);         // making rule: z = x - 20

solver.solve(callback);

// callback is called when model is complete and symbols are set
function callback(valid) {
  // valid indicates whether a model was found or not
  if (valid) {
    console.log("constraints: ");
    console.log("x = 3y + 2z");
    console.log("z < y < x / 2");
    console.log("z = x - 20");
    console.log("\nSolutions:")
    console.log("x: " + x.getValue());
    console.log("y: " + y.getValue());
    console.log("z: " + z.getValue());
  }
}
