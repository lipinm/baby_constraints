var code = require("../src/constraints");
var solver = code.default();

var arr = [];
for (var i = 0; i < 81; i++) {
  arr.push(solver.Int());
}

arr[0].setVal(5);
arr[1].setVal(3);
arr[4].setVal(7);
arr[9].setVal(6);
arr[12].setVal(1);
arr[13].setVal(9);
arr[14].setVal(5);
arr[19].setVal(9);
arr[20].setVal(8);
arr[25].setVal(6);
arr[27].setVal(8);
arr[31].setVal(6);
arr[35].setVal(3);
arr[36].setVal(4);
arr[39].setVal(8);
arr[41].setVal(3);
arr[44].setVal(1);
arr[45].setVal(7);
arr[49].setVal(2);
arr[53].setVal(6);
arr[55].setVal(6);
arr[60].setVal(2);
arr[61].setVal(8);
arr[66].setVal(4);
arr[67].setVal(1);
arr[68].setVal(9);
arr[71].setVal(5);
arr[76].setVal(8);
arr[79].setVal(7);
arr[80].setVal(9);

for (var v = 1; v <= 9; v++) {
  for (var i = 0; i < 9; i++) {
    var rule = false;
    for (var j = 0; j < 9; j++) {
      rule = rule | arr[i*9 + j] == v;
    }
    solver.assert(rule);
  }
}

for (var v = 1; v <= 9; v++) {
  for (var j = 0; j < 9; j++) {
    var rule = false;
    for (var i = 0; i < 9; i++) {
      rule = rule | arr[i*9 + j] == v;
    }
    solver.assert(rule);
  }
}

for (var v = 1; v <= 9; v++) {
  for (var l = 0; l < 3; l++) {
    for (var k = 0; k < 3; k++) {
      var rule = false;
      for (var j = 0; j < 3; j++) {
        for (var i = 0; i < 3; i++) {
          rule = rule | arr[27*l + 9*j + 3*k + i] == v;
        }
      }
      solver.assert(rule);
    }
  }
}

printGrid();
console.log("\n");

solver.solve(callback);

function printGrid() {
  for (var i = 0; i < 9; i++) {
    var row = "";
    for (var j = 0; j < 9; j++) {
      if (arr[i*9 + j].getValue() == null) {
        row += "_ ";
      } else {
        row += arr[i*9 + j].getValue() + " ";
      }
    }
    console.log(row);
  }
}

function callback(valid) {
  if (valid) {
    for (var i = 0; i < 9; i++) {
      var row = "";
      for (var j = 0; j < 9; j++) {
        row += arr[i*9 + j].getValue() + " ";
      }
      console.log(row);
    }
  }
}
