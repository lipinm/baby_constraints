var code = require("../src/constraints");
var solver = code.default();

var arr = [];
for (var i = 0; i < 81; i++) {
  arr.push(solver.Int());
}

for (var v = 1; v <= 9; v++) {
  for (var i = 0; i < 9; i++) {
    var rule = false;
    for (var j = 0; j < 9; j++) {
      rule = rule | arr[i*9 + j] == v;
    }
    solver.assert(rule);
  }
}

for (var v = 1; v <= 9; v++) {
  for (var j = 0; j < 9; j++) {
    var rule = false;
    for (var i = 0; i < 9; i++) {
      rule = rule | arr[i*9 + j] == v;
    }
    solver.assert(rule);
  }
}

for (var v = 1; v <= 9; v++) {
  for (var l = 0; l < 3; l++) {
    for (var k = 0; k < 3; k++) {
      var rule = false;
      for (var j = 0; j < 3; j++) {
        for (var i = 0; i < 3; i++) {
          rule = rule | arr[27*l + 9*j + 3*k + i] == v;
        }
      }
      solver.assert(rule);
    }
  }
}

solver.solve(callback);

function callback(valid) {
  if (valid) {
    for (var i = 0; i < 9; i++) {
      var row = "";
      for (var j = 0; j < 9; j++) {
        row += arr[i*9 + j].getValue() + " ";
      }
      console.log(row);
    }
  }
}
